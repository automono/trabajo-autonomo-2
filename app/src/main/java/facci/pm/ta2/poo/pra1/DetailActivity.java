package facci.pm.ta2.poo.pra1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {
    Button btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");




        // INICIO - CODE6
        //pregunta 3.1
        // en la siguiente linea de codigo se esta recibiendo como parametro el object_id
        String object_id = getIntent().getStringExtra("object_id");
        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>() {
            //En esta linea se accede a las propiedades del metodo done
            @Override
            public void done(DataObject object, DataException e) {
                if(e == null){
                    //se obtiene todos los datos
                    TextView title = (TextView)findViewById(R.id.nombre);
                    TextView precio = (TextView)findViewById(R.id.precio);
                    ImageView thumbnail = (ImageView)findViewById(R.id.thumbnail);
                    TextView descripcion = (TextView)findViewById(R.id.descripcion);
                    // se muestra los datos de los objetos
                    title.setText((String) object.get("name"));
                    precio.setText((String) object.get("price"));
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));

                }
                else {

                }

            }
        });


        // FIN - CODE6



    }

}
